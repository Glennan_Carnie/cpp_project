DOCKER TEST PROJECT
===================

This is a simple C++ project to allow the user to test Docker build facilities.

The project provides a simple SCons script for construction (others - CMake, Meson, etc. - will be added in later revisions.

There are two ways to build this project:

## As an application
This is the default build configuration.  All the code in the /src folder will be compiled into a single executable.

By default the output executable is named `demo`, and is in the `build` folder.

The executable file name and output directory can be changed by editing the `SConstruct` file (if required)

```
# SConstruct file

# Project outputs
#
output_dir    = '#/build/'
exe_name      = 'demo'
```

To build the project, invoke SCons from the command line:
```
$ scons
```

## As a Test-Drived Development project
The project can be configured to run as a TDD executable.  In this case, the project's main() function
is removed and instead any test cases defined will be run.

The project uses the Feabhas test framework. This test framework is based on the CATCH2 test framework and contains a basic set of unit test facilities. Instructions on its use can be found in `/feabhas_test/readme.md`.

To build for TDD, invoke SCons with the `--test` flag:
```
$ scons --test
```
When you run the executable, any defined test cases will be run, instead of the application's (normal) behaviour.

A set of simple test cases has been included in the project in the file `/src/module.cpp` so you can see the basic structure and syntax.

NOTE:

The test configuration removes the file that contains `main()` (by default `main.cpp`).  As a consequence, any other code in `main.cpp` will be omitted from the project. Therefore, do not include any definitions in `main.cpp` that are used by other parts of the project (not that you should do such things anyway!).

The file containing main() can be changed, if required, by editing the file `/src/SConscript`
```
#/src/SConscript

main_src = 'main.cpp'
```

## Visual Studio code setup
The project includes a workspace configuration and several build tasks for Visual Studio Code.

To open the workspace, either double-click on the `demo.code-workspace` file, or launch VS Code and select `File -> Open Workspace`.

The demo project is cofigured to work with the `gcc10-scons` Docker container.  You _must_ have pulled this container from DockerHub before attempting to build the project.

To save having to invoke the container from the command line on every build (which is tedious) a set of build tasks has been configured These can be accessed from VS Code by pressing `ctrl + shift + b`.  This will bring up the list of build tasks (with the most-recent task at the top of the list).

Three tasks are available:
* Build the application on Docker
* Build the TDD project
* Launch an interactive Docker shell

The third option will leave the container running and present a (linux) command line.  This command line can be used to invoke SCons, or run the executable.

_Note:  invoking a build task will automatically save any unsaved files in the project; so you don't have to remember to do so!_
