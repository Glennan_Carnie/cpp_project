// ----------------------------------------------------------------------------------
// feabhas_test.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------
// Outstanding work
//
// - In-range matcher for doubles required
// - Bit-pattern matcher required
// - Test harness has raw pointer for its test container. Replace with unique_ptr
//
// ----------------------------------------------------------------------------------

#ifndef FEABHAS_TEST_H_
#define FEABHAS_TEST_H_

#include <functional>
#include <string>
#include <vector>
#include <array>
#include <iostream>
#include <iomanip>
#include <string>
#include <regex>


// ------------------------------------------------------------------------------------------------
//
namespace Feabhas {

    namespace Test {

        // ------------------------------------------------------------------------------------------------
        // Test result exception types
        //
        enum class Test_result { pass, fail, ignore };

        // Abstract base class for all exceptions that could be
        // thrown by the test framework.
        //
        class Exception {
        public:
            Exception() = default;

            Exception(Test_result exception_type) :
                exc_type { exception_type }
            {
            }

            Exception(Test_result exception_type, std::string str) :
                exc_type { exception_type },
                text     { str }
            {
            }

            virtual void report() const = 0;

            Test_result type() const
            {
                return exc_type;
            }

        protected:
            const std::string& as_string() const
            {
                return text;
            }

        private:
            Test_result exc_type { };
            std::string text     { };
        };


        // Basic test failure case
        //
        class Fail : public Exception {
        public:
            Fail() : Exception { Test_result::fail }
            {
            }

            Fail(std::string str) : Exception { Test_result::fail, str }
            {
            }

            void report() const override
            {
                std::cout << "Assertion failed! ";
                std::cout << Exception::as_string();
                std::cout << std::endl;
            }
        };


        // Condition mismatch records a difference between expected
        // and actual values
        // 
        template <typename T>
        class Condition_mismatch : public Exception {
        public:
            template <typename U>
            Condition_mismatch(std::string str, U&& expected_value, U&& actual_value) :
                Exception { Test_result::fail, std::move(str) },
                expected  { std::forward<U>(expected_value) },
                actual    { std::forward<U>(actual_value) }
            {
            }

            void report() const override
            {
                std::cout << "Test failed : " << Exception::as_string() << std::endl;
                std::cout << "   expected : " << expected               << std::endl;
                std::cout << "   actual   : " << actual                 << std::endl;
                std::cout << std::endl;
            }

        private:
            T expected;
            T actual;
        };
        
        // Deduction guide
        template <typename T>
        Condition_mismatch(std::string, T, T) -> Condition_mismatch<T>;

        // Not a failure case, but used when a test isn't
        // supposed to be run.
        //
        class Ignore : public Exception {
        public:
            Ignore() : Exception { Test_result::ignore }
            {
            }

            Ignore(std::string str) : Exception { Test_result::ignore, str }
            {
            }

            void report() const override
            {
                std::cout << "Test ignored: ";
                std::cout << Exception::as_string();
                std::cout << std::endl;
            }
        };


        // Indicates a test abort call
        //
        class Abort : public Exception {
        public:
            using Exception::Exception;

            void report() const override
            {
                std::cout << "Test aborted: ";
                std::cout << Exception::as_string();
                std::cout << std::endl;
            }
        };


        // ------------------------------------------------------------------------------------------------
        //
        class Test_case;
        
        // The test harness class acts ast the container for all test cases.  It allows
        // static test case objects to register at program start, so clients aren't required
        // to do any object creation of their own.
        //
        class Test_harness {
        public:
            static inline void add_test_case(Test_case& test);
            inline void run_all();
            inline void results() const;
            friend inline std::ostream& operator<<(std::ostream& os, const Test_harness& harness);

        private:
            using test_container = std::vector<Test_case*>;

            static test_container* tests;
            
            unsigned int num_passed  { 0 };
            unsigned int num_failed  { 0 };
            unsigned int num_ignored { 0 };
        };


    // ------------------------------------------------------------------------------------------------
    // 
        // A test case encapsulates the function that is the client's test.  That is, when the client
        // creates a TEST_CASE, some macro magic is used to automagically instantiate a Test_case object
        // and register it with the Test_harness.
        //
        class Test_case {
        public:
            using test_function = void(*)();

            Test_case(test_function fn, std::string str) : 
                test { fn },
                description { std::move(str) } 
            {
                Test_harness::add_test_case(*this);
            }

            inline Test_result operator()();
            inline const char* as_string() const;

            friend inline std::ostream& operator<<(std::ostream& os, const Test_case& test_case);

        private:
            test_function test { nullptr };
            std::string description { };
            Test_result result { Test_result::ignore };
        };


    // ------------------------------------------------------------------------------------------------
    //
        void Test_harness::run_all()
        {
            for (auto test_ptr : *tests) {
                auto result = (*test_ptr)();

                switch (result) {
                    case Test_result::pass    : ++num_passed;  break;
                    case Test_result::fail    : ++num_failed;  break;
                    case Test_result::ignore  : ++num_ignored; break;
                    default:                                   break;
                }
            }

            results();
        }


        void Test_harness::add_test_case(Test_case& test)
        {
            if (!tests) {
                tests = new test_container { };
            }
            tests->push_back(&test);
        }


        std::ostream& operator<<(std::ostream& os, const Test_harness& harness)
        {
            os << "\n";
            os << "=========================================================="  << "\n";
            os << "TEST RESULTS"                                                << "\n";
            os << "Tests run     : " << harness.tests->size()                   << "\n";
            os << "Tests passed  : " << harness.num_passed                      << "\n";
            os << "Tests failed  : " << harness.num_failed                      << "\n";
            os << "Tests ignored : " << harness.num_ignored                     << "\n";
            os << "\n";
            
            return os;
        }


        void Test_harness::results() const
        {
            std::cout << *this << std::endl;
        }


    // ------------------------------------------------------------------------------------------------
    //
        Test_result Test_case::operator()()
        {
            std::cout << "\n----------------------------------------------------------\n";
            std::cout << "Running test: " << description << std::endl;

            try {
                test();
                return Test_result::pass;
            }
            catch (Exception& assertion_exception) {
                assertion_exception.report();
                return assertion_exception.type();
            }
        }


        const char* Test_case::as_string() const
        {
            switch (result) {
                case Test_result::pass:     return "pass";
                case Test_result::fail:     return "fail";
                case Test_result::ignore:   return "ignore";
                default:                    return "unknown";
            }
        }


        std::ostream& operator<<(std::ostream& os, const Test_case& test_case)
        {
            os << std::left;
            os << "Test: " << std::setw(75) << test_case.description << ": ";
            os << test_case.as_string();
            return os;
        }

    // ------------------------------------------------------------------------------------------------
    // 
        // --------------------------------------------------------------
        // Base classes for core types.  These are used to disambiguate
        // verify_that() calls.
        //
        // Value-type comparisons (equals, greater-than, less-than, etc.)
        //
        class Condition_Type { };

        // Callable types (these must be invoked before their
        // results can be checked)
        //
        class Callable_Type { };

        // Exception handling types (these will invoking a callable
        // type and check for any thrown exception)
        //
        class Exception_Type { };

        // Enablers; for use with type-specific functions
        //
        template <typename U>
        using is_callable = typename std::enable_if<std::is_base_of<Callable_Type, typename std::decay<U>::type>::value, bool>::type;

        template <typename U>
        using is_condition = typename std::enable_if<std::is_base_of<Condition_Type, typename std::decay<U>::type>::value, bool>::type;

        template <typename U>
        using is_value = typename std::enable_if<!std::is_base_of<Condition_Type, typename std::decay<U>::type>::value, bool>::type;

        template <typename U>
        using is_exception = typename std::enable_if<std::is_base_of<Exception_Type, typename std::decay<U>::type>::value, bool>::type;


        // --------------------------------------------------------------
        // Anything
        //
        class Anything : public Condition_Type {
        public:
            template <typename T>
            void verify(const T&) const
            {
            }

            template <typename T>
            bool operator()(const T&) const
            {
                return true;
            }
        };


        static Anything anything [[maybe_unused]]  { };

        // --------------------------------------------------------------
        // Equality
        //
        template <typename T>
        class Equals : public Condition_Type {
        public:
            template <typename U>
            Equals(U&& value) :
                match_value{ std::forward<U>(value) }
            {
            }

            void verify(const T& actual) const
            { 
                bool test_passed  { actual == match_value };
                if (!test_passed) {
                    throw Condition_mismatch { "Values not equal", match_value, actual };
                }
            }

        private:
            T match_value;
        };


        template <>
        class Equals<double> : public Condition_Type {
        public:
            explicit Equals(double value) :
                match_value{ value }
            {
            }

            void verify(const double& actual) const
            {
                bool test_passed { std::abs(match_value - actual) <= min_difference };
                if (!test_passed) {
                    throw Condition_mismatch { "Values not equal", match_value, actual };
                }
            }

            Equals<double>& within(double offset)
            {
                min_difference = offset;
                return *this;
            }

        private:
            double match_value;
            double min_difference{ 0.0 };
        };


        template <typename T>
        inline decltype(auto) equal_to(T&& expected)
        {
            return Equals<typename std::decay<T>::type>{ std::forward<T>(expected) };
        }


        template <typename T>
        inline decltype(auto) equals(T&& expected)
        {
            return equal_to(std::forward<T>(expected));
        }


        inline auto equal_to(Anything)
        {
            return Anything { };
        }


        inline auto equals(Anything anything)
        {
            return equal_to(anything);
        }


        inline auto equals(const char* str)
        {
            return Equals<std::string> { str };
        }


        inline auto equal_to(const char* str)
        {
            return Equals<std::string> { str };
        }

        // --------------------------------------------------------------
        // Greater-than
        //
        template <typename T>
        class Greater_than : public Condition_Type {
        public:
            Greater_than(T value) :
                match_value{ value }
            {
            }

            void verify(const T& actual) const
            {
                bool test_passed { actual > match_value };
                if (!test_passed) {
                    throw Condition_mismatch { "Actual value not greater-than expected value", match_value, actual };
                }
            }

        private:
            T match_value;
        };


        template <typename T>
        inline decltype(auto) greater_than(T&& expected)
        {
            return Greater_than<typename std::decay<T>::type>{ std::forward<T>(expected) };
        }


        inline auto greater_than(Anything)
        {
            return Anything { };
        }


        inline auto greater_than(const char* str)
        {
            return Greater_than<std::string> { str };
        }

        // --------------------------------------------------------------
        // Less-than
        //
        template <typename T>
        class Less_than : public Condition_Type {
        public:
            Less_than(T value) :
                match_value{ value }
            {
            }

            void verify(const T& actual) const
            {
                bool test_passed { actual < match_value };
                if (!test_passed) {
                    throw Condition_mismatch { "Actual value not less-than expected value", match_value, actual };
                }
            }

        private:
            T match_value;
        };


        template <typename T>
        inline decltype(auto) less_than(T&& expected)
        {
            return Less_than<typename std::decay<T>::type>{ std::forward<T>(expected) };
        }


        inline auto less_than(Anything)
        {
            return Anything { };
        }


        inline auto less_than(const char* str)
        {
            return Less_than<std::string> { str };
        }

        // --------------------------------------------------------------
        // Same object
        //
        template <typename T>
        class Same_object : public Condition_Type {
        public:
            Same_object(T& object) :
                identity { &object }
            {
            }

            void verify(const T& actual) const
            {
                bool test_passed { &actual == identity };
                if (!test_passed) {
                    throw Condition_mismatch { "Objects have different identities", identity, &actual };
                }
            }

        private:
            T* identity;
        };


        template <typename T>
        class Same_object<T*> : public Condition_Type {
        public:
            Same_object(T* ptr) :
                identity{ ptr }
            {
            }

            void verify(T* const ptr_obj) const
            {
                bool test_passed { ptr_obj == identity };
                if (!test_passed) {
                    throw Condition_mismatch { "Objects have different identities", identity, ptr_obj };
                }
            }

        private:
            T* identity;
        };


        template <typename T>
        inline Same_object<T> same_object_as(T& expected)
        {
            return Same_object<T> { expected };
        }


        // --------------------------------------------------------------
        // Range
        // Ranges only work with value types
        //
        template <typename T>
        class In_range : public Condition_Type {
        public:
            In_range(T low_val, T high_val) :
                low  { low_val },
                high { high_val }
            {
            }

            void verify(const T& actual) const
            {
                bool test_passed  { (actual >= low) && (actual <= high) };
                if (!test_passed) {
                    throw Condition_mismatch { "Value outside range", low, actual }; 
                }
            }

        private:
            T low  { };
            T high { };
        };


        template <typename T>
        inline decltype(auto) in_range(T&& from, T&& to)
        {
            return In_range<typename std::decay<T>::type> { 
                std::forward<T>(from), 
                std::forward<T>(to) 
            };
        }

        // --------------------------------------------------------------
        // TO DO:  In-range for floating point types
        // --------------------------------------------------------------

        // --------------------------------------------------------------
        // Regular expression
        // Regular expressions only work with strings
        //
        class Regular_expression : public Condition_Type {
        public:
            Regular_expression(std::string str) :
                search_pattern { std::move(str) }
            {
            }


            void verify(const std::string& actual) const
            {
                std::regex reg_ex { search_pattern };
                auto test_passed = std::regex_match(actual, reg_ex);
                if (!test_passed) {
                    throw Condition_mismatch { "Value does not match regular expression", search_pattern, actual };
                }
            }

        private:
            std::string search_pattern { };
        };


        inline auto reg_ex(const char* search_str)
        {
            return Regular_expression { search_str };
        }


        inline auto regular_expression(const char* search_str)
        {
            return Regular_expression { search_str };
        }

        // --------------------------------------------------------------
        // 'is' functions
        //
        template <typename T, is_value<T> = true>
        inline decltype(auto) is(T&& value)
        {
            return Equals<std::decay_t<T>>{ std::forward<T>(value) };
        }


        template <typename T, is_condition<T> = true>
        inline decltype(auto) is(T&& condition)
        {
            return std::forward<T>(condition);
        }


        inline auto is(const char* str)
        {
            return Equals<std::string> { str };
        }


        template <typename T, is_value<T> = true>
        inline decltype(auto) returns(T&& value)
        {
            return Equals<std::decay_t<T>>{ std::forward<T>(value) };
        }


        template <typename T, is_condition<T> = true>
        inline decltype(auto) returns(T&& matcher)
        {
            return std::forward<T>(matcher);
        }


        inline auto returns(const char* str)
        {
            return Equals<std::string> { str };
        }


        // --------------------------------------------------------------
        // Function invoker
        //
        template <typename Func_Ty>
        class Invoker : public Callable_Type {
        public:
            Invoker(Func_Ty&& f) : fn { std::forward<Func_Ty>(f) }
            {
            }

            auto operator()()
            {
                return fn();
            }

        private:
            Func_Ty fn { };
        };

        template <typename T>
        Invoker(T) -> Invoker<T>;

        template <typename Func_Ty, typename Tuple_Ty, std::size_t ... Index>
        auto make_invoker_from_tuple(Func_Ty&& fn, Tuple_Ty&& t, std::index_sequence<Index...>)
        {
            return Invoker { std::bind(std::forward<Func_Ty>(fn), std::get<Index>(t)...) };
        }


        template <typename Func_Ty, typename Tuple_Ty>
        auto invoking(Func_Ty&& fn, Tuple_Ty&& params)
        {
            return make_invoker_from_tuple(
                std::forward<Func_Ty>(fn), 
                std::forward<Tuple_Ty>(params),
                std::make_index_sequence<std::tuple_size<Tuple_Ty>::value> { }
            );
        }


        template <typename Func_Ty>
        auto invoking(Func_Ty&& fn)
        {
            return Invoker { std::forward<Func_Ty>(fn) };
        }

        template <typename... T>
        auto with_args(T&&... arg)
        {
            return std::make_tuple(std::forward<T>(arg)...);
        }


        // --------------------------------------------------------------
        // Throwing exceptions
        //
        template <typename T>
        class Thrown : public Exception_Type {
        public:
            template <typename Func_Ty>
            void verify(Func_Ty&& fn) const
            {
                bool correct_exception { false };
                bool exception_thrown  { false };

                try {
                    fn();
                }
                catch(T&)  { 
                    exception_thrown  = true;
                    correct_exception = true; 
                }
                catch(...) { 
                    exception_thrown  = true;
                    correct_exception = false;
                }
                
                if (!exception_thrown)  throw Fail { "No exception thrown" };
                if (!correct_exception) throw Fail { "Incorrect exception thrown" };
            }

        };


        template <typename T>
        decltype(auto) throws_exception(const T&)
        {
            return Thrown<typename std::decay<T>::type> { };
        }

        #define throws(T) throws_exception(T { })
        
        // --------------------------------------------------------------
        // Assertion functions.
        //
        template <
            typename Value_Ty, 
            typename Condition_Ty,
            is_value<Value_Ty>         = true, 
            is_condition<Condition_Ty> = true
        >
        inline void verify_that(const Value_Ty& actual_value, const Condition_Ty& condition)
        {
            condition.verify(actual_value);
        }


        template <
            typename Callable_Ty, 
            typename Condition_Ty, 
            is_callable<Callable_Ty>   = true,
            is_condition<Condition_Ty> = true
        >
        inline void verify_that(Callable_Ty&& invoked_fn, const Condition_Ty& condition)
        {
            condition.verify(invoked_fn());
        }


        template <
            typename Callable_Ty, 
            typename Exception_Ty, 
            is_callable<Callable_Ty>   = true,
            is_exception<Exception_Ty> = true
        >
        inline void verify_that(Callable_Ty&& invoked_fn, const Exception_Ty& exception_condition)
        {
            // if (exception_from(std::forward<Callable_Ty>(fn)) == false) {
            //     throw Fail { };
            // }

            exception_condition.verify(std::forward<Callable_Ty>(invoked_fn));
        }


        inline void abort_test(Test_result result)
        {
            throw Abort { result };
        }


        inline void abort_test(Test_result result, std::string str)
        {
            throw Abort { result, std::move(str) };
        }


        inline void ignore_test()
        {
            throw Ignore { };
        }


        inline void ignore_test(std::string str)
        {
            throw Ignore { std::move(str) };
        }

    } // namespace Test
} // namespace Feabhas

// ------------------------------------------------------------------------------------------------
//
#define CONCAT(x, y)        x ## y
#define CONCAT2(x, y)       CONCAT(x, y)
#define TEST_FUNC_NAME      CONCAT2(test_func_, __LINE__)
#define TEST_CASE_NAME      CONCAT2(test_case_, __LINE__)

// Generate test cases when testing is enabled
// otherwise leave them as plain-old functions
//
#ifdef TESTING_ENABLED

    #define TEST_CASE(desc)                                                         \
        static void TEST_FUNC_NAME();                                               \
        static Feabhas::Test::Test_case TEST_CASE_NAME { TEST_FUNC_NAME, desc };    \
        void TEST_FUNC_NAME() 


    #define INIT_TEST_FRAMEWORK(test_harness_name)                                                      \
        Feabhas::Test::Test_harness test_harness_name { };                                              \
        Feabhas::Test::Test_harness::test_container* Feabhas::Test::Test_harness::tests { nullptr }     \

    #define RUN_ALL_TESTS(test_harness_name)    \
        test_harness_name.run_all()

#else
    
    #define TEST_CASE(desc)                          void TEST_FUNC_NAME() 
    #define INIT_TEST_FRAMEWORK(test_harness_name)
    #define RUN_ALL_TESTS(test_harness_name) 

#endif

// If the client wants a main() automatically generated
//
#ifdef TEST_FRAMEWORK_GENERATE_MAIN
    
    INIT_TEST_FRAMEWORK(feabhas_test_framework);

    int main()
    {
        RUN_ALL_TESTS(feabhas_test_framework);
    }

#endif


#endif // FEABHAS_TEST_H_
