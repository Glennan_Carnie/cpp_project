// test_main.cpp
//
// In normal usage, tests will be automatically registered and run 
// when your program executes.  To do this feabhas_test can provide 
// its own main() function.
// To enable this, you must define the macro `TEST_FRAMEWORK_GENERATE_MAIN` 
// in one - and only one! - source file.
//
// This file must only be include in the project build if you want tests
// to run (rather than normal application execution).  If you include
// this file you will get a link error for duplicate definitions of main()
//
#define TEST_FRAMEWORK_GENERATE_MAIN
#include "feabhas_test.h"