#!python
import os

# Project outputs
#
output_dir    = '#/build/'
exe_name      = 'demo'


# Compiler configuration
#
cxx_compiler_flags = [ '-Wall', '-Wextra', '-std=c++17'  ]
c_compiler_flags   = [ '-std=gnu11' ]
optimisation_flags = [ '-g3', '-Og' ]
macro_definitions  = [ 'TRACE_DISABLED' ]
linker_flags       = [ ]
libraries          = ['pthread' ]

# ----------------------------------------------------------------------------
# Configure SCons
#

# Add a test configuration.  If scons is run with the
# '--test' option use the feabhas_test main() instead
# of the application's main().  The resulting executable
# will execute all defined test cases.
#
AddOption(
    '--test',
    action  = 'store_true',
    help    = 'Build for test only',
    default = False
)

# Get all the source and include paths (and any configurations)
# from project sub-directories
#
env           = Environment(ENV = os.environ)
source_files  = []
include_dirs  = []
var_dir       = output_dir

SConscript(
    dirs = 'src',
    variant_dir = var_dir,
    exports = [
        'env', 
        'source_files', 
        'include_dirs', 
        'c_compiler_flags',
        'cxx_compiler_flags',
        'macro_definitions'  
    ],
    duplicate = False
)

SConscript(
    dirs = 'feabhas_test', 
    exports = [
        'env', 
        'source_files', 
        'include_dirs', 
        'c_compiler_flags',
        'cxx_compiler_flags',
        'macro_definitions'  
    ]
)

env.Append(CPPPATH    = include_dirs)
env.Append(LIBS       = libraries)
env.Append(CXXFLAGS   = cxx_compiler_flags)
env.Append(CXXFLAGS   = optimisation_flags)
env.Append(CFLAGS     = c_compiler_flags)
env.Append(LINKFLAGS  = linker_flags)
env.Append(CPPDEFINES = macro_definitions)

env.Program(target = output_dir + exe_name, source = source_files)

