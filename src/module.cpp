#include "feabhas_test.h"

using namespace Feabhas::Test;

int func(int a)
{
    return a + 1;
}


TEST_CASE("Basic test")
{
    ignore_test("Not written yet");
}


TEST_CASE("Test that fails")
{
    verify_that(invoking(func, with_args(10)), returns(12));
}


TEST_CASE("Test that passes")
{
    verify_that(invoking(func, with_args(10)), returns(greater_than(10)));
}


TEST_CASE("New syntax")
{
    int a { 20 };
    int expected { 21 };


    verify_that(
        invoking(func, with_args(a)), 
        returns(greater_than(expected))
    );   
}