cpp_files=$(find src \( -name *.h -o -name *.cpp \) -exec echo /opt/{} \;)

docker run \
    --rm \
    --privileged=true \
    --volume ${PWD}:/opt \
    saschpe/clang-format -i -style=file ${cpp_files}